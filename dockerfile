FROM  php:7.2-alpine

COPY ./composer.sh /tmp/

RUN chmod +x /tmp/*.sh && sh /tmp/composer.sh && mv composer.phar /usr/local/bin/composer

RUN apk update && apk upgrade \
  && apk add --no-cache git \
  && apk add --no-cache --virtual .build-deps build-base autoconf \
  && rm -rf /usr/share/man

RUN pecl install xdebug \
  && docker-php-ext-enable xdebug

CMD ["php", "-a"]
